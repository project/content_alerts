CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Content Alerts is module to show alerts of new content is posted on the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/content_alerts

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/content_alerts


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Content Alerts module as you would normally install a
   contributed Drupal module. 


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to admin/content-alerts-settings to configure.
    3. Navigate to admin/structure/block to place the block (Content Alert Modal) in region like footer.


MAINTAINERS
-----------

 * Karuna Sharma - https://www.drupal.org/u/karunasharmansi

Supporting organization:
 * Net Solutions - https://www.drupal.org/net-solutions
