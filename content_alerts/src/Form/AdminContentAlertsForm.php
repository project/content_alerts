<?php

namespace Drupal\content_alerts\Form;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Form\FormStateInterface;
use Drupal\content_alerts\Controller\AlertsModalController;

/**
 * Provides a confirmation form for cancelling multiple user accounts.
 */
class AdminContentAlertsForm extends FormBase {

  
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'admin_content_alerts_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        
        $bundles = node_type_get_names();
        $alerts_config = \Drupal::config('content_alerts.settings');
        $def_bundles_arr = $alerts_config->get('content_type'); 
    

        foreach ($def_bundles_arr as $key => $value) {
            $def_bundles[$key] = ($value == 1)?$key:$value;
        }
         
        $form['content_type'] = [
            '#title' => 'Select a Content Type',
            '#type' => 'checkboxes',
			'#description' => 'Please select the content type for content alerts.',
            '#options' => $bundles,
            '#default_value' => isset($def_bundles)?$def_bundles:[],
           
        ];
        
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save changes')
        ];

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
		$data = $form_state->getUserInput();
		$content_type = $data['content_type'];
		$form_data = [];
		
        foreach ($data['content_type'] as $key => $values) {
            $form_data[$key] = is_string($values)?1:0;
        }
        $config = \Drupal::service('config.factory')->getEditable('content_alerts.settings');
        $config->set('content_type', $form_data);    
        $config->save();
        drupal_set_message('Your Configurations has been saved.','status');
    }
}