<?php

namespace Drupal\content_alerts\Form;
namespace Drupal\content_alerts\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;


/**
 * Class AlertsModalController.
 * Drupal\content_alerts\Controller
 */ 

class AlertsModalController extends ControllerBase {

/**
 * Get Form 
 * @return $form
 */

   public function notificationsSetting(){

        $form_class = '\Drupal\content_alerts\Form\AdminContentAlertsForm';
        return $build['form'] = \Drupal::formBuilder()->getForm($form_class);
    }
    
}