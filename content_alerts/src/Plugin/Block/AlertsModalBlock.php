<?php

namespace Drupal\content_alerts\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 *
 * @Block(
 *   id = "Content Alert Modal",
 *   admin_label = @Translation("Content Alert Modal"),
 *   category = @Translation("Block to be show the content alerts in modal"),
 * )
 */
class AlertsModalBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() {
		$user = \Drupal::currentUser()->id(); 
		if($user != 0){
			$result = db_query("SELECT id,user_id, title FROM content_alerts WHERE NOT FIND_IN_SET($user, user_id)")->fetchAll();
			$msg_new ='';
			foreach($result as $row){
				$msg = $row->title;
				$msg1 = drupal_set_message($msg, 'status');
				$update = $row->user_id.','.$user; 
				 \Drupal::database()->update('content_alerts')
				   ->condition('id', $row->id)
				  ->fields([
				 'user_id' =>  $update
				  ])->execute();    
				$msg_new .='<li>'.$msg.'</li>';
			}   
		}  
		return [
			'#theme' => 'content_alerts_modal',
			'#data' => $msg_new,
			'#cache' => [
				'max-age' => 0
			],
						 
		];
	}
}  